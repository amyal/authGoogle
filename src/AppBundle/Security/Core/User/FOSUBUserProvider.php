<?php
/**
 * Created by PhpStorm.
 * User: rachid
 * Date: 11/08/2018
 * Time: 11:12
 */

namespace AppBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class FOSUBUserProvider
 * @package AppBundle\Security\Core\User
 */
class FOSUBUserProvider extends BaseClass
{


    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        $email = $response->getEmail();
        // If the user does not exist in DB we create it
        if (null === $user || null === $username) {
            // create new user here
            $user = $this->userManager->createUser();
            $user->setGoogleId($username);
            $user->setGoogleAccessToken($response->getAccessToken());
            // to connect directly after registring

            $user->setUsername($username);
            $user->setPassword($username);
            $user->setEmail($email);
            $user->setEnabled(1);
            $user->setFirstName($response->getFirstName());
            $user->setLastName($response->getLastName());
            $this->userManager->updateUser($user);
            return $user;
        }
        //if the user exists
        $user = parent::loadUserByOAuthUserResponse($response);
        $serviceName = $response->getResourceOwner()->getName();
        $nbrConnection = (integer)$user->getNbConnection() + 1 ;
        $user->setNbConnection($nbrConnection);
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';
        //update access token
        $user->$setter($response->getAccessToken());
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();
        //on connect - get the access token and the user ID

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->setGoogleId(null);
            $previousUser->setGoogleAccessToken(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->setGoogleId($username);
        $user->setGoogleAccessToken($response->getAccessToken());
        $nbrConnection = (integer)$user->getNbConnection() + 1 ;
        $user->setNbConnection($nbrConnection);
        $this->userManager->updateUser($user);
    }


}